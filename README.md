# Lorena Matrix Helpers
Javascript helper libraries for the Lorena Matrix libraries 

|Branch|Pipeline|Coverage|
|:-:|:-:|:-:|
|[`master`](https://gitlab.com/caelum-tech/Lorena/lorena-matrix-helpers/tree/master)|[![pipeline status](https://gitlab.com/caelum-tech/Lorena/lorena-matrix-helpers/badges/master/pipeline.svg)](https://gitlab.com/caelum-tech/Lorena/lorena-matrix-helpers/commits/master)|[![coverage report](https://gitlab.com/caelum-tech/Lorena/lorena-matrix-helpers/badges/master/coverage.svg)](https://gitlab.com/caelum-tech/Lorena/lorena-matrix-helpers/commits/master)|

## Run tests

Install dependencies
```bash
npm i
```

Run the tests
```bash
npm test
```

## Usage
```bash
npm install --save-dev @caelum-tech/verifier-helpers

```

```JavaScript
const verifierClient = require('@caelum-tech/verifier-helpers')
```

## See also
[Lorena Matrix Client](https://gitlab.com/caelum-tech/Lorena/lorena-matrix-client)
[Lorena Matrix Daemon](https://gitlab.com/caelum-tech/Lorena/lorena-matrix-daemon)
