const fetch = require('node-fetch')
const uuidv4 = require('uuid/v4')

/**
 * Create a matrix user
 *
 * @param {string} matrixServerURL Matrix home server
 * @param {string} username optional username to apply; otherwise uses a generated UUID
 * @param {string} password optional password to apply; otherwise uses a generated UUID
 * @returns {JSON} user information
 */
async function createMatrixUser (matrixServerURL, username = undefined, password = undefined) {
  if (!username) {
    username = uuidv4()
  }
  if (!password) {
    password = uuidv4()
  }

  const result = await createMatrix('user', matrixServerURL, username, password)
  result.username = username
  result.password = password
  return result
}

/**
 * Create a Matrix guest user
 *
 * @param {string} matrixServerURL Matrix home server
 * @returns {JSON} user information
 */
async function createMatrixGuest (matrixServerURL) {
  return createMatrix('guest', matrixServerURL)
}

/**
 * Create a Matrix user or guest
 *
 * @param {string} kind 'guest' or 'user'
 * @param {string} matrixServerURL Matrix home server
 * @param {string} username optional username (unnecessary for 'guest')
 * @param {string} password optional password (unnecessary for 'guest')
 * @returns {JSON} user information
 */
async function createMatrix (kind, matrixServerURL, username = undefined, password = undefined) {
  const body = { username, password, auth: { type: 'm.login.dummy' } }
  const response = await fetch(`${matrixServerURL}/_matrix/client/r0/register?kind=${kind}`,
    { method: 'post', mode: 'cors', referrerPolicy: 'origin-when-cross-origin', body: JSON.stringify(body), headers: { 'Content-Type': 'application/json' } })
  return response.json()
}

/**
 * Create random account to use during testing
 *
 * @param {string} matrixServerURL Matrix base server
 * @returns {Promise} user information
 */
async function createMatrixTestUser (matrixServerURL) {
  return createMatrixUser(matrixServerURL)
}

module.exports = { createMatrixGuest, createMatrixUser, createMatrixTestUser }
