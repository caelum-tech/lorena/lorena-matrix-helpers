const fetch = require('node-fetch')

/**
 * Log in to the specified Matrix server with the specified username and password
 *
 * @param {string} matrixServerUrl - Matrix server URL
 * @param {string} user - @username:matrix.server
 * @param {string} password - password
 * @returns {JSON} access_token
 */
async function loginMatrixUser (matrixServerURL, user, password) {
  const body = { user, password, type: 'm.login.password' }
  const response = await fetch(matrixServerURL + '/_matrix/client/api/v1/login',
    { method: 'post', mode: 'cors', referrerPolicy: 'origin-when-cross-origin', body: JSON.stringify(body), headers: { 'Content-Type': 'application/json' } })
  const json = await response.json()
  return json
}

module.exports = { loginMatrixUser }
