/**
 * Format a Matrix userId according to the standard
 *
 * @param {string} matrixServer Matrix home server
 * @param {string} userName Username
 * @returns {string} formatted userId
 */
function buildMatrixUserId (matrixServer, userName) {
  return `@${userName}:${matrixServer}`
}

module.exports = { buildMatrixUserId }
