const fetch = require('node-fetch')

/**
 * Attempt to join a room in order to consent to the Matrix Terms of Service, which is enforced
 * on the matrix.org home server for guest users.
 * The default room is set up for the matrix.org matrix server, so if you're using a different
 * home server, you'll need a room set up for that.
 *
 * @param {string} matrixBaseURL - Matrix server URL
 * @param {string} accessToken - Matrix access token
 * @param {string} room optional room to use when trying to see if we need to accept TOS
 * @returns {boolean} for presumed success
 */
async function consentToMatrixTOS (matrixBaseURL, accessToken, room = '#lorena-tos:matrix.org') {
  const result = await joinMatrixRoomAsGuest(matrixBaseURL, room, accessToken, false)
  // If it successfully joined a room then the TOS was probably already agreed to.
  // If it failed with the errcode it probably submitted the form post to provide consent,
  // but we can't check the result because of CORS, so we assume success.
  return (result.room_id !== undefined || result.errcode === 'M_CONSENT_NOT_GIVEN')
}

/**
 * Join the specified room or alias as a guest account, handling any consent required
 *
 * @param {string} matrixBaseURL - Matrix server URL
 * @param {string} roomIdOrAlias - !random:matrix.server or #alias:matrix.server
 * @param {string} accessToken - Matrix access token
 * @param {boolean} retry - retry after failing due to not having achieved consent?
 * @returns {JSON} Success: `room_id`, Failure: `errcode`
 */
async function joinMatrixRoomAsGuest (matrixBaseURL, roomIdOrAlias, accessToken, retry = true) {
  // This uses obsolete Matrix API because the new API `/_matrix/client/api/r0/rooms/#roomAlias/join` only works with
  // the real room name, not the room alias for which it always returns M_FORBIDDEN.
  let response = await fetch(`${matrixBaseURL}/_matrix/client/api/v1/join/${encodeURIComponent(roomIdOrAlias)}?access_token=${accessToken}`,
    { method: 'post', mode: 'cors', referrerPolicy: 'origin-when-cross-origin', body: JSON.stringify({}), headers: { 'Content-Type': 'application/json' } })

  let responseJSON = await response.json()

  // If the server is requiring that the guest answer affirmatively to a consent form,
  // fill it in and post it.
  if (responseJSON.errcode === 'M_CONSENT_NOT_GIVEN') {
    // The original URL has some of the necessary parameters to be sent as a form post.
    // Add an additional needed parameter to send along.
    const url = new URL(responseJSON.consent_uri)
    const params = url.searchParams
    params.append('v', '1.0')

    // Get the base URL without parameters (I can't believe the URL class doesn't have a method for this)
    const consentURL = responseJSON.consent_uri.substr(0, responseJSON.consent_uri.indexOf('?'))

    // Submit a form post with consent
    await fetch(consentURL,
      {
        method: 'post',
        mode: 'no-cors',
        body: params,
        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
        referrer: { consentURL }
      })
    // we can't look at the result because of CORS.  Of course.

    if (retry) {
      // try joining again!
      response = await fetch(`${matrixBaseURL}/_matrix/client/api/v1/join/${encodeURIComponent(roomIdOrAlias)}?access_token=${accessToken}`,
        { method: 'post', mode: 'cors', referrerPolicy: 'origin-when-cross-origin', body: JSON.stringify({}), headers: { 'Content-Type': 'application/json' } })
      responseJSON = await response.json()
    }
  }
  return responseJSON
}

/**
 * Leave the specified room
 *
 * @param {string} matrixBaseURL - Matrix server URL
 * @param {string} roomId - !random:matrix.server (not aliases!)
 * @param {string} accessToken - Matrix access token
 * @returns {JSON} ok
 */
async function leaveMatrixRoom (matrixBaseURL, roomId, accessToken) {
  const response = await fetch(`${matrixBaseURL}/_matrix/client/r0/rooms/${roomId}/leave?access_token=${accessToken}`,
    { method: 'post', mode: 'cors', referrerPolicy: 'origin-when-cross-origin', body: JSON.stringify({}), headers: { 'Content-Type': 'application/json' } })
  return response
}

module.exports = { consentToMatrixTOS, joinMatrixRoomAsGuest, leaveMatrixRoom }
