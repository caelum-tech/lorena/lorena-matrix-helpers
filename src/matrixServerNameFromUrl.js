/**
 * Returns the Matrix server name from a matrix URL
 *
 * @param {string} baseUrl - Matrix server URL
 * @returns {string} serverName
 */
function matrixServerNameFromUrl (baseUrl) {
  return baseUrl.replace(/^http(s)?:\/\//i, '')
}

module.exports = { matrixServerNameFromUrl }
