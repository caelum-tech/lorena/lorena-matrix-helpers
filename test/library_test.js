const index = require('../index.js')
const chai = require('chai')

// Configure chai
chai.should()

describe('Library', function () {
  it('should export methods', () => {
    index.buildMatrixUserId.should.not.be.undefined
    index.consentToMatrixTOS.should.not.be.undefined
    index.createMatrixGuest.should.not.be.undefined
    index.createMatrixUser.should.not.be.undefined
    index.createMatrixTestUser.should.not.be.undefined
    index.joinMatrixRoomAsGuest.should.not.be.undefined
    index.leaveMatrixRoom.should.not.be.undefined
    index.loginMatrixUser.should.not.be.undefined
    index.matrixServerNameFromUrl.should.not.be.undefined
    index.random.should.not.be.undefined
  })
})
