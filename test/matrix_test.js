const helpers = require('../index.js')
const chai = require('chai')
const uuidv4 = require('uuid/v4')

const serverName = 'matrix.caelumlabs.com'
const baseUrl = `https://${serverName}`

// Configure chai
chai.should()

let userId, password

describe('matrixServerNameFromUrl', function () {
  it('should get matrix server name from URL', async () => {
    helpers.matrixServerNameFromUrl(baseUrl).should.eq(serverName)
  })
})

describe('buildMatrixUserId', function () {
  it('should build user ID from username and server name', async () => {
    helpers.buildMatrixUserId(helpers.matrixServerNameFromUrl(baseUrl), 'example').should.eq(`@example:${serverName}`)
  })
})

describe('create matrix users', function () {
  it('should create matrix test user', async () => {
    const result = await helpers.createMatrixTestUser(baseUrl)
    result.user_id.should.not.be.empty
    result.home_server.should.eq(serverName)
    result.access_token.should.not.be.empty
    result.device_id.should.not.be.empty
    result.username.should.not.be.empty
    result.password.should.not.be.empty
  })

  it('should create matrix user', async () => {
    const username = uuidv4()
    password = uuidv4()
    // on our server this works, but on matrix.org it does not.
    const result = await helpers.createMatrixUser(baseUrl, username, password)
    result.user_id.should.not.be.empty
    result.home_server.should.eq(serverName)
    result.access_token.should.not.be.empty
    result.device_id.should.not.be.empty
    result.username.should.eq(username)
    result.password.should.eq(password)
    // save these for next test
    userId = result.user_id
    password = result.password
  })
})

describe('loginMatrixUser', function () {
  // Disabled because this does not work with matrix.org, only with our custom server
  it('should log in with a new matrix test user', async () => {
    const session = await helpers.loginMatrixUser(baseUrl, userId, password)
    session.access_token.should.not.be.undefined
    session.device_id.should.not.be.undefined
    session.user_id.should.be.eq(userId)
    session.home_server.should.eq(baseUrl.substr(baseUrl.indexOf('://') + 3))
    session.home_server.should.eq(serverName)
  })
})

describe('Guest user access', function () {
  let guest, roomId

  it('should create matrix guest', async () => {
    guest = await helpers.createMatrixGuest(baseUrl)
    guest.user_id.should.not.be.empty
    guest.home_server.should.eq(serverName)
    guest.access_token.should.not.be.empty
    guest.device_id.should.not.be.empty
  })

  it('should join a room as a guest', async () => {
    const result = await helpers.joinMatrixRoomAsGuest(baseUrl, '#lorena-matrix-helpers-test:matrix.caelumlabs.com', guest.access_token)
    result.room_id.should.not.be.undefined
    roomId = result.room_id
  })

  it('should leave a room', async () => {
    const result = await helpers.leaveMatrixRoom(baseUrl, roomId, guest.access_token)
    result.ok.should.be.true
  })

  it('should not join a bogus room', async () => {
    const result = await helpers.joinMatrixRoomAsGuest(baseUrl, '#bogus-matrix-lorena-xxx-room:matrixcaelumlabs.com', guest.access_token)
    result.errcode.should.be.oneOf(['M_NOT_FOUND', 'M_UNKNOWN'])
  })
})

describe('Guest user TOS access', function () {
  // We have to use the regular Matrix server to test this, because TOS isn't configured on our Matrix server
  const matrixBaseURL = 'https://matrix.org'

  it('should join a room as a guest despite TOS barrier', async () => {
    const guest = await helpers.createMatrixGuest(matrixBaseURL)
    const result = await helpers.joinMatrixRoomAsGuest(matrixBaseURL, '#lorena-lobby-ci:matrix.org', guest.access_token, true)
    result.room_id.should.not.be.undefined
  })

  it('should create a matrix guest and validate TOS', async () => {
    const guest = await helpers.createMatrixGuest(matrixBaseURL)
    const result = await helpers.consentToMatrixTOS(matrixBaseURL, guest.access_token /*, '#lorena-tos:matrix.org' */)
    result.should.be.true
  })
})
