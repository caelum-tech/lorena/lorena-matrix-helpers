const random = require('../src/random.js')
const chai = require('chai')

// Configure chai
chai.should()

describe('Random', function () {
  it('should create random IDs', () => {
    const id1 = random.makeid(11)
    const id2 = random.makeid(11)
    id1.should.not.be.empty
    id2.should.not.be.empty
    id1.should.not.eq(id2)
  })
})
