
const random = require('./src/random.js')
const { buildMatrixUserId } = require('./src/buildMatrixUserId.js')
const { createMatrixGuest, createMatrixUser, createMatrixTestUser } = require('./src/createMatrixUser.js')
const { consentToMatrixTOS, joinMatrixRoomAsGuest, leaveMatrixRoom } = require('./src/matrixRooms.js')
const { loginMatrixUser } = require('./src/loginMatrixUser.js')
const { matrixServerNameFromUrl } = require('./src/matrixServerNameFromUrl.js')

module.exports = { buildMatrixUserId, consentToMatrixTOS, createMatrixGuest, createMatrixUser, createMatrixTestUser, joinMatrixRoomAsGuest, leaveMatrixRoom, loginMatrixUser, matrixServerNameFromUrl, random }
